Nothing to see here, move along. Seriously.
-------------------------------------------

This repository contains sensitive information, encrypted with my GPG
key. You are not expected to have any uses of content of this
repository; the only reason it is world-readable is more simple recovery
should my computer suddenly die.

Visit [https://passwordstore.org](https://passwordstore.org) to learn about
tool, used to create this repository.
